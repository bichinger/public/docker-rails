# Rails on Phusion Passenger™ Docker Base Image

## Description

This image is intended as a base image for all rails applications. It is based on an official Phusion Passenger™ Community Edition image
which itself is based on the `phusion-baseimage` (which adds some nice container startup features), both licensed under the MIT license.
It adds: `nodejs LTS`, `yarn` and `bundler` (for the default Ruby).
This image automatically does the following things:

* (ONBUILD) copy the (optional) file `nginx/app-custom.conf` from the child images' git root into nginx's config dir, it will then be included at the end of the app.conf (nginx configuration top level)
* (ONBUILD) copy the (optional but almost always used) file `nginx/app-env-vars-custom.conf` from the child images' git root into nginx's config dir to make nginx aware of app-specific ENV vars
* (ONBUILD) copy the (optional) file `nginx/server-custom.conf` from the child images' git root into nginx's config dir, it will then be included in the *server* section in the app.conf
* (ONBUILD) copy the (optional) file `nginx/passenger-enterprise-license` from the child images' git root into `/etc/` inside the container
* (ONBUILD) if build argument `NGINX_CONFIG_FOLDER` is used, copy the above files from that main folder instead of `nginx/`
* (ONBUILD) if build argument `NGINX_CONFIG_SUBFOLDER` is used, copy the above files from that subfolder inside the main nginx folder *over* the existing ones (=replace them)
* (ONBUILD) check for presence of build argument `PASSENGER_DOWNLOAD_TOKEN` and file `/etc/passenger-enterprise-license` and install Phusion Passenger™ Enterprise if both are present (and valid ;)
* (ONBUILD) set the complete access_log directive (excluding the semicolon at the end) to what's set in build argument `NGINX_ACCESS_LOG` (default: `off`, meaning NO access log! set to an empty string to turn it on again)
* (ONBUILD) changes the access_log format to what's set in build argument `NGINX_ACCESS_LOG_FORMAT` (default: a variation of `combined` format anonymizing the IP addresses named `anonymized`). note that this will have no effect when using the `NGINX_ACCESS_LOG` argument (which is set to `off` by default!)
* (ONBUILD) changes the error_log level to what's set in build arguments `NGINX_ERROR_LOG` (default: `stderr` to log to STDERR) and `NGINX_ERROR_LOG_LEVEL` (default: empty, uses nginx default)
* (ONBUILD) copy the source code from the `APP_SOURCE_FOLDER` argument (default: dir `src` in git root) into the app home (`$APP_HOME`)
* (ONBUILD) determines current git revision and stores it in file `$APP_GIT_REVISION_FILE` (default: `/GIT_REVISION`) and appends reading that file into environment variable `$APP_GIT_REVISION` via `/etc/environment`
* start nginx and wait for requests on port 80 on all IPs that will be handled by nginx.

If you should ever need to overwrite (!) the standard nginx configs included in the base image, you can do so as the container does this:

* (ONBUILD) copy the (optional) file `nginx/app.conf` from the child images' git root into nginx's sites-enabled dir, overwriting (!) the default nginx config from the container. You have to manually include app-custom.conf and server.conf if you still want to include them.
* (ONBUILD) copy the (optional) file `nginx/app-env-vars.conf` from the child images' git root into nginx's config dir, overwriting (!) the default nginx config from the container.
* (ONBUILD) copy the (optional) file `nginx/server.conf` from the child images' git root into nginx's config dir, overwriting (!) the default nginx config from the container. You have to manually include server-custom.conf if you still want to include it.
* (ONBUILD) if build argument `NGINX_CONFIG_FOLDER` is used, copy the above files from that main folder instead of `nginx` (`nginx` is the default if no argument is supplied)
* (ONBUILD) if build argument `NGINX_CONFIG_SUBFOLDER` is used, the above files from that subfolder are also copied over the existing ones, just like with the custom files described above.

Note you should avoid overwriting these configs as things might break when updating the base image later.

## Available arguments to configure this image

* `APP_SOURCE_FOLDER` by default, the Rails app is expected in `src` subfolder in the repository, use `--build-arg APP_SOURCE_FOLDER=<relative folder>` to set this variable to the folder's name (no slashes or dots at beginning or end except if you want to set it to the repository root (`.`), e.g. `docker build --build-arg APP_SOURCE_FOLDER=. .`)
* `NGINX_CONFIG_FOLDER` the nginx/Passenger configuration files are copied from the `nginx` folder in the repository. This argument changes that folder for the nginx configuration. E.g. `--build-arg NGINX_CONFIG_FOLDER=build/nginx` to specify to find all the nginx configuration files in `build/nginx` folder.
* `NGINX_CONFIG_SUBFOLDER` the nginx/Passenger configuration files are copied from the subfolder in the `nginx` folder (if not reconfigured with `NGINX_CONFIG_FOLDER`) in the repository. If you need to have different files being used for specific environments (like testing and production), use `--build-arg NGINX_CONFIG_SUBFOLDER=<folder name inside nginx/ folder>` to specify which folder inside the `nginx` subfolder should be looked into to *additionally copy over* configuration files from. Files copied directly from the `nginx` folder (if any) will be overwritten with these files.
* `NGINX_ACCESS_LOG_FORMAT` this base image adds a new log format named `anonymized` which is based on `combined` but anonymizes the IP addresses to conform with GDPR-requirements. This new format is the new default but you can set this argument to [other formats](http://nginx.org/en/docs/http/ngx_http_log_module.html#access_log). You should consider changing the log format in your server config (file `server-custom.conf`) though if you need more control.
* `NGINX_ERROR_LOG_LEVEL` the log level for the main error_log directive is empty by default, but you can set this argument to set a level from [the error_log directive documentation](http://nginx.org/en/docs/ngx_core_module.html#error_log)

## Available environment vars for child images

* `$APP_HOME` the working directory of the rails app inside the container
* `$APP_USER` the user the application will run under
* `$APP_GROUP` the user group the application will run under
* `$APP_GIT_REVISION_FILE` the full path of a file containing only the *full git commit SHA* of the built app

## Available environment vars in the running container (in the shell, not in the app)

* `$APP_GIT_REVISION` the full git commit SHA of the built app (also see `$APP_GIT_REVISION_FILE`)

## nginx and Environment variables

By default, nginx does not pass on any environment variables to the application except `TZ`. You can,
however, specify with variables should be passed to the app. By default, the docker-rails image
passes these:

* `PASSENGER_APP_ENV` this will always be set into `RAILS_ENV`, `RACK_ENV`, `NODE_ENV` and `WSGI_ENV`, is *production* by default and can be overridden 
* `BUNDLE_GEMFILE`
* `RAILS_RELATIVE_URL_ROOT`
* `DATABASE_URL`
* `RAILS_CACHE_ID`
* `RAILS_APP_VERSION`
* `SECRET_KEY_BASE`
* `RAILS_MAX_THREADS`
* `RAILS_LOG_TO_STDOUT`
* `RAILS_SERVE_STATIC_FILES`
* `RAILS_MASTER_KEY`
* `APP_GIT_REVISION_FILE`
* `CONTAINER_HOST_IDENTIFIER`

You can specify additional environment variables by putting them in a file called `nginx/app-env-vars-custom.conf` in your
application root directory.

The format is simple (but don't forget the `;`s at the end):

```
env MY_ENVIRONMENT_VARIABLE;
env ANOTHER_ENVIRONMENT_VARIABLE;
```

## Using Phusion Passenger™ Enterprise

In order to replace the Community Edition with the Enterprise Edition of Phusion Passenger™, add your license file
(named `passenger-enterprise-license`) to the `nginx/` dir in git root and build your image with the build argument
`PASSENGER_DOWNLOAD_TOKEN` and the value of your actual Phusion Passenger™ download token.

  As the pricing model depends on RAM, you can use the variable `CONTAINER_HOST_IDENTIFIER` to propagate some host
  identifier to your containers so Phusion recognizes the used RAM correctly.

## Changelog
### Version 4.41.0
* updated to Passenger Docker image 3.1.2 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* introduce Ruby 3.4 support (image tags ending with/containing `-ruby34`)

### Version 4.40.1
* `NGINX_ACCESS_LOG` set empty by default, meaning the access log has the default value (/var/log/nginx/access.log)
* `NGINX_ERROR_LOG` set empty by default, meaning the error log has the default value (/var/log/nginx/error.log)
* symlink nginx /var/log/nginx/access.log and .../error.log files to /dev/stdout and /dev/stderr respectively

### Version 4.40.0
* updated to Passenger Docker image 3.1.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* Note: Ubuntu 24.04 and nginx 1.24 are now used

### Version 4.39.0
* updated to Passenger Docker image 3.0.7 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.38.0
* change build from Docker-in-Docker to Kaniko
* updated to Passenger Docker image 3.0.6 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.37.0
* _removed_ installation of packages for Puppeteer images (`*-puppeteer-rubyXY`) that are not listed in the [official installation instructions](https://pptr.dev/guides/system-requirements) (currently version 22.10.0):
  * ca-certificates
  * fonts-liberation
  * gconf-service
  * libappindicator1
  * libfontconfig1
  * libgcc1
  * libgconf-2-4
  * libgdk-pixbuf2.0-0
  * libgtk-3-0
  * lsb-release
  * wget
  * xdg-utils
* updated to Passenger Docker image 3.0.5 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* Ruby 3.0 support dropped

### Version 4.36.0
* updated to Passenger Docker image 3.0.3 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.35.0
* updated to Passenger Docker image 3.0.1 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* fix RVM directory's group ownership so app user is able to install all gems

### Version 4.34.0
* updated to Passenger Docker image 3.0.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* introduce Ruby 3.3 support (image tags ending with/containing `-ruby33`)
* update Node.js version from 18 LTS to 20 LTS

### Version 4.33.0
* updated to Passenger Docker image 2.6.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* Passenger image updated Ubuntu from 20.04 LTS to 22.04 LTS
* Ruby 2.7 support dropped - image is now Ruby 3 only
* everything in public/assets*/ and public/packs*/ folders is now subject to maximum caching

### Version 4.32.1
* installation of nodejs updated (version still 18 LTS)
* add bash alias `berk` to root and app user to run `bundle exec rake`
* updated nginx brotli module to include brotli version 1.1.0

### Version 4.32.0
* updated to Passenger Docker image 2.5.1 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* nodejs was updated from version 16 to 18 in the Passenger Docker image, so we did the same here
* add bash aliases `be` and `ber` to root and app user to run `bundle exec` and `bundle exec rails` respectively
* add Ubuntu `webp` package for webp support

### Version 4.31.0
* updated to Passenger Docker image 2.5.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* introduce Ruby 3.2 support (image tags ending with/containing `-ruby32`) 

### Version 4.30.0
* updated to Passenger Docker image 2.4.1 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* Ruby 2.6 support dropped

### Version 4.29.0
* updated to Passenger Docker image 2.3.1 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* skip installing old bundler versions (only the latest one is installed)

### Version 4.28.1
* fix extracting git revision (safe directory policy)

### Version 4.28.0
* updated to Passenger Docker image 2.3.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.27.0
* updated to Passenger Docker image 2.2.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* introduce Ruby 3.1 support (image tags ending with/containing `-ruby31`)

### Version 4.26.1
* introduces build argument `NGINX_CONFIG_FOLDER` to set the base nginx configuration folder (defaults to "nginx") where the subfolders are located 

### Version 4.26.0
* updated to Passenger Docker image 2.1.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.25.0
* updated to Passenger Docker image 2.0.1 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)
* update Node.js version from 14 to 16 LTS

### Version 4.24.0
* split image into two separate images: a *base* image (tagged `-base` at the end) containing all the files and the main image adding the ONBUILD commands.
This way, own intermediate base images can be created without losing the ONBUILD commands.

### Version 4.23.0
* updated to Passenger Docker image 2.0.0 (see https://github.com/phusion/passenger-docker/blob/master/CHANGELOG.md)

### Version 4.22.1
* introduces build argument `NGINX_ACCESS_LOG` to set the whole string following the `access_log` directive excluding the finishing semicolon (see https://nginx.org/en/docs/http/ngx_http_log_module.html#access_log)
* introduces build argument `NGINX_ERROR_LOG` to set the error logfile (see https://nginx.org/en/docs/ngx_core_module.html#error_log)
* `NGINX_ACCESS_LOG` is set to `off` by default, meaning access log is turned off completely! set to an empty string to turn it on again and restore the old behaviour.
* `NGINX_ERROR_LOG` is set to `stderr` by default, meaning the error log is redirected to stderr! set to an empty string to restore the old behaviour.

### Version 4.21.0
* update gem system on image build (updates rubygems, bundler)

### Version 4.20.0
* upgrade passenger-docker baseimage to version 1.0.19

### Version 4.19.0
* upgrade passenger-docker baseimage to version 1.0.15

### Version 4.18.0
* removed image tags `<version>` and `<version>-puppeteer`. Use one of the `<version>-rubyXY` and `<version>-puppeteer-rubyXY` tags instead (with `XY` being one of 26, 27 or 30).
* fix Phusion Passenger™ Enterprise installation on focal

### Version 4.17.0
* upgrade passenger-docker baseimage to version 1.0.13
* added Ruby version *3.0.0* to be built and tagged (`4.17.0[-puppeteer]-ruby30`) 

### Version 4.16.2
* add shared-mime-info package for recent breaking changes of mimemagic gem

### Version 4.16.1
* preinstall bundler of major versions 1, 2 and the latest

### Version 4.16.0
* builds an additional version of the image with Ruby 2.7. From now on, this image will be built with the following tags:
    * `<version>` and `<version>-ruby26` with Ruby 2.6
    * `<version>-puppeteer` and `<version>-puppeteer-ruby26` with Ruby 2.6 and required packages for Puppeteer
    * `<version>-ruby27` with Ruby 2.7
    * `<version>-puppeteer-ruby27` with Ruby 2.7 and required packages for Puppeteer
    
  Current Ruby versions are *2.6.6* and *2.7.2*. Ruby 3 will follow once Phusion releases the corresponding docker image.
  
  For compatibility reasons, you should switch to the images with the Ruby version tags (e.g. `4.16.0-ruby26` instead of `4.16.0`).

### Version 4.15.0
* upgrade passenger-docker baseimage to version 1.0.12 (introduces Ubuntu 20.04 and nginx 1.18, drops Ruby 2.3 support)
* update Node.js version from 12 to 14 LTS

### Version 4.14.1
* hide Passenger version in header

### Version 4.14.0
* upgrade passenger-docker baseimage to version 1.0.11

### Version 4.13.0
* upgrade passenger-docker baseimage to version 1.0.10

### Version 4.12.9.2 (late hotfix for this version only!)
* turn off apt's ssl certificate validation

### Version 4.12.9
* set nginx's server_tokens to off, preventing it to expose its version

### Version 4.12.8
* update Puppeteer requirements for that image version

### Version 4.12.7
* no new features but builds an additional version of the image with requirements for Puppeteer (tagged `<version>-puppeteer`)

### Version 4.12.6
* maintenance release

### Version 4.12.5
* update yarn apt-repository key onbuild to prevent expiry
* added brotli module and enabled it (static asset serving uses .br extension)

### Version 4.11.3
* rebuild to update yarn repository key

### Version 4.11.2
* introduce build arguments `NGINX_ACCESS_LOG_FORMAT` and `NGINX_ERROR_LOG_LEVEL` to control the default loggers
* anonymized access_log (IP anonymization) is the default now!

### Version 4.10.0
* upgrade passenger-docker baseimage to version 1.0.9

### Version 4.9.0
* introduce build argument `NGINX_CONFIG_SUBFOLDER` for environment-specific nginx/Passenger configuration

### Version 4.8.4
* suppress sending of etag on prebuilt asset/pack resources

### Version 4.8.3
* add `CONTAINER_HOST_IDENTIFIER` variable

### Version 4.8.2
Maintenance release, oldest available version due to registry move

### Version 4.8.1
* fix permissions of `tmp/` directory

### Version 4.8.0
* upgrade passenger-docker baseimage to version 1.0.8
* set passenger app type to rack (instead of autodetect)
* include server-custom.conf at the end of server.conf rather than in the middle of the file

### Version 4.7.1
* GitLab CI build configuration pinned to Docker 19.03.1 images
* exports ENV variable APP_GROUP in addition to APP_USER
* create tmp/restart.txt file with correct user permissions
* upgrade passenger-docker baseimage to version 1.0.7

### Version 4.6.2
* added `jq` to installed apt packages

### Version 4.6.1
* add `APP_GIT_REVISION_FILE` env var to nginx 

### Version 4.6.0
* added optional Phusion Passenger™ Enterprise installation

### Version 4.5.1
* fix presence of `APP_GIT_REVISION` env (added to baseimage init process)

### Version 4.5.0
* remove faulty (was not filled with git commit SHA) `APP_GIT_REVISION` env from container
* introduce `APP_GIT_REVISION_FILE` env (default: `/GIT_REVISION`)
* set env `APP_GIT_REVISION` in _running_ container via `/etc/environment`
